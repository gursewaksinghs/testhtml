﻿using System.ComponentModel;
using Xamarin.Forms;

namespace Test_HTML
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            lblHTML.Text = $"<ol><li><p><em><strong><u>Description two</u></strong></em></p></li><li><p><em><strong><u>Description two</u></strong></em></p></li><li><p><em><strong><u>Description two</u></strong></em></p></li></ol><ul><li><p><em><strong><u>Description</u></strong></em></p></li><li><p><em><strong><u>twoDescription</u></strong></em></p></li><li><p><em><strong><u>woDescription</u></strong></em></p></li><li><p><em><strong><u>woDescription</u></strong></em></p></li><li><p><em><strong><u>woDescriptionsddsd twoDescription two</u></strong></em></p></li></ul><p></p>";
        }
    }
}
